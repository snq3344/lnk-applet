## 免费分享

简洁、实用圈子前端，极大地降低了产品的研发和技术成本。

主打简约精美UI，助力企业实现私域运营，最终实现快速积累客户、会员数据分析、智能转化客户、有效提高销售、提升会员运营的效果。

我们深知做好一件事已经不容易，运营好站点更不容易，我们欢迎更多有社区站点运营的朋友，加入我们的讨论交流群，一同探索交流。

## 安装、交流、讨论、BUG修复请加群，微信 nzkd01


## 体验
后台：https://demo.lnksns.vip/admin/index.html  

帐号密码：admin
## 小程序
![小程序](https://lfs.k.topthink.com/lfs/37e0adb7a6a2560fa549f554b583981ff6018bcc5b8f8b610f46d6996129fdad.dat)

## 系统亮点
UI精美，流畅体验

精致美观的UI页面，少即是多，用户可轻松上手操作，带来简单、舒适的用户体验，精心打磨，只为给你带来不一样的产品。

框架为uni-app可适配多端应用。

## 功能列表
1、动态支持发文字、图集、视频；

2、回复支持emoji表情，支持二级回复；

3、支持快速获取微信头像、昵称；

4、支持调用微信手机号验证；

5、支持每日签到积分；

6、支持选择圈子；

7、更多功能请下载发现。。。。

![输入图片说明](https://gitee.com/snq3344/lnksns/raw/master/md/T1.png)
![输入图片说明](https://gitee.com/snq3344/lnksns/raw/master/md/T2.png)

## 小程序
> 下载程序
> 解压后进入lnk-applet目录
> 使用HBuilder X 构建、发布

## 版权信息
🔐 下载、安装学习使用无需付费。
本项目包含的第三方源码和二进制文件之版权信息另行标注。

## 特别鸣谢
💕 感谢巨人提供肩膀，排名不分先后
- [Thinkphp](http://www.thinkphp.cn/)
- [FastAdmin](https://gitee.com/karson/fastadmin)
- [Vue](https://github.com/vuejs/core)
- [vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)
- [Element Plus](https://github.com/element-plus/element-plus)
- [TypeScript](https://github.com/microsoft/TypeScript)
- [vue-router](https://github.com/vuejs/vue-router-next)
- [vite](https://github.com/vitejs/vite)
- [LnkAdmin](https://gitee.com/DengJe/LiteAdmin)
- [Pinia](https://github.com/vuejs/pinia)
- [qinghang-qz](https://ext.dcloud.net.cn/plugin?id=15190)
- [Axios](https://github.com/axios/axios)
- [nprogress](https://github.com/rstacruz/nprogress)
- [screenfull](https://github.com/sindresorhus/screenfull.js)
- [mitt](https://github.com/developit/mitt)
- [sass](https://github.com/sass/sass)
- [wangEditor](https://github.com/wangeditor-team/wangEditor)
- [echarts](https://github.com/apache/echarts)
- [vueuse](https://github.com/vueuse/vueuse)